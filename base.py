from utils import Site

class Parser:
    """
    Parser de objetos.
    """
    def __init__(self, content):
        self._content = content
    
    @property
    def content(self):
        return self._content


# TODO: Convertir en abstract.
class Crawler(Site):
    """
    Recorre para encontrar urls de documentales de una web.
    """
    paginator = ''

    def _get_url_by_page(self, page):
        """
        Retorna la url de la página url base + numero de página.

        Keywords params:
        page -- int.
        """
        pass

    def _get_url_by_element(self, element):
        """
        Retorna la url de un elemento de la lista por página.

        Keywords params:
        element -- string.
        """
        pass

    def _get_documentaries_by_page(self, url):
        """
        Retorna una lista de urls de documentales de una página.

        Keywords params:
        url -- string.
        """
        pass

    @property
    def documentaries(self):
        documentaries = []
        for i in self.pages:
            url = self._get_url_by_page(i)
            per_page = self._get_documentaries_by_page(url)
            documentaries += per_page
        return documentaries

    @property
    def last_page(self):
        pass
