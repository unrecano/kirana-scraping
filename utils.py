import datetime
import os
from pymongo import MongoClient
import requests
from dotenv import load_dotenv
from bs4 import BeautifulSoup

load_dotenv(verbose=True)

class Site:
    """
    Sitios web y atributos.
    """
    def __init__(self, url):
        self._url = url
        self._content = self.get_content(self.url)

    @staticmethod
    def get_content(url):
        try:
            print(url)
            request = requests.get(url)
            return BeautifulSoup(request.content, 'lxml')
        except Exception as e:
            raise e

    @property
    def body(self):
        return self.content.body

    @property
    def content(self):
        return self._content

    @property
    def title(self):
        return self.content.title.string

    @property
    def url(self):
        return self._url

class ScrapingDB:
    # Database credentials.
    _DB_USER = os.getenv('DB_USER')
    _DB_PASS = os.getenv('DB_PASS')
    _DB_HOST = os.getenv('DB_HOST')
    _URI = f"mongodb+srv://{_DB_USER}:{_DB_PASS}@{_DB_HOST}/"

    def __init__(self):
        self._client = MongoClient(f"{self._URI}?retryWrites=true&w=majority")
        self._db = self._client.scraping

    def insert_documentary(self, documentary):
        """
        Insertar documental en base de datos.

        documentary -- dict.
        """
        exists = self._db.documentaries.find_one({'url': documentary['url']})
        if exists:
            return exists
        documentary['inserted_at'] = datetime.datetime.now()
        return self._db.documentaries.insert_one(documentary)

    def insert_error(self, error):
        """
        Insertar error en base de datos.

        error -- dict.
        """
        error['date'] = datetime.datetime.now()
        return self._db.errors.insert_one(error)
