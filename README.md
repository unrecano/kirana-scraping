Kirana scraping
===

Realizar scraping a los sitios de documentales:

- [DocumentaryTube](http://www.documentarytube.com)
- [Documentary Mania](https://www.documentarymania.com/)

Sitios pendientes:

- [Documentary Addict](https://documentaryaddict.com)
- [Documentary Heaven](https://documentaryheaven.com)
- [Top Documentaries Films](https://topdocumentaryfilms.com)
- [Watch Documentaries](https://watchdocumentaries.com)

Sitios no habilitados (por ahora):

- [Documentary Mania](http://www.documentarymania.com)

Pendientes:

- Usar scrapy.