from base import Parser, Crawler
from utils import ScrapingDB, Site

url_base = "http://www.documentarytube.com"

class DocumentaryParser(Parser):
    """
    Objeto que representa un documental.
    """
    @property
    def title(self):
        main = self.content.find('div', {'class': 'container mt35 mb35'})
        return main.find('h1').text.strip()
    
    @property
    def description(self):
        text = ""
        main = self.content.find('div', {'class': 'container mt35 mb35'})
        for p in main.find_all('p', {'class': 'MsoNoSpacing'}):
            text = text + f' {p.text}'
        return text.strip()
        
    @property
    def tags(self):
        tags = self.content.find('li', {"class": "category"})
        return [tag.text for tag in tags.find_all('a') if tag.text.strip()]

    @property
    def attrs(self):
        return {
                "title": self.title,
                "description": self.description,
                "tags": self.tags
                }

class DocumentariesCrawler(Crawler):
    paginator = 'page={}&per-page=24'

    def _get_url_by_page(self, page):
        return f'{url_base}/videos?{self.paginator.format(page + 1)}'

    def _get_url_by_element(self, element):
        return element.find('div', class_='box-title').find('a').get('href')

    def _get_documentaries_by_page(self, url):
        content = self.get_content(url)
        elements = content.find('div', {'class': 'list-view'})\
                          .find_all('div', {'class': 'panel'})
        return [self._get_url_by_element(element) for element in elements]

    @property
    def pages(self):
        element = self.content.find('ul', {"class": "pagination"})
        pages = [a.find('a') for a in element if a.find('a') != None]
        last_page = int(pages[-1].get('data-page'))
        return range(0, int(last_page) + 1)

class DocumentariesScraping:
    """
    Corre scraping al sitio y guarda la información.
    """
    @staticmethod
    def run():
        site = "DocumentaryTube"
        db = ScrapingDB()
        crawler = DocumentariesCrawler(url_base + '/videos')
        for url in crawler.documentaries:
            try:
                content = Site.get_content(url)
                documentary = DocumentaryParser(content.body).attrs
                documentary['url'] = url
                documentary['site'] = site
                db.insert_documentary(documentary.attr)
            except Exception as e:
                error = {
                        "url": url,
                        "message": str(e)
                        }
                db.insert_error(error)
                continue